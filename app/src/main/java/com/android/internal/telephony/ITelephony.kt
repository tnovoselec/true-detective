package com.android.internal.telephony

/*
 * Needed to obtain proper service from TelephonyManager via reflection. Should become publicly available in next release of Android
 * http://www.nikola-breznjak.com/blog/android/make-native-android-app-can-block-phone-calls/
 */
interface ITelephony {

    fun endCall(): Boolean

    fun answerRingingCall()

    fun silenceRinger()
}