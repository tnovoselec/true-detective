package com.tnovoselec.truedetective.business.call

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.telephony.TelephonyManager
import dagger.android.AndroidInjection
import javax.inject.Inject

/*
 *  Intercept incoming calls and ask CallHandler to decide what to do with them
 */
class CallReceiver : BroadcastReceiver() {

    @Inject
    lateinit var callHandler: CallHandler

    override fun onReceive(context: Context?, intent: Intent?) {
        AndroidInjection.inject(this, context)

        try {
            val state = intent?.getStringExtra(TelephonyManager.EXTRA_STATE)
            val number = intent?.getStringExtra(TelephonyManager.EXTRA_INCOMING_NUMBER)

            callHandler.handleCall(state, number)
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}