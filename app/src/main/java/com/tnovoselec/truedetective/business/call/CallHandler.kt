package com.tnovoselec.truedetective.business.call

import android.content.Context
import android.content.Intent
import android.telephony.PhoneNumberUtils
import android.telephony.TelephonyManager
import android.util.Log
import com.android.internal.telephony.ITelephony
import com.tnovoselec.truedetective.model.Call
import com.tnovoselec.truedetective.model.CallDescription
import com.tnovoselec.truedetective.model.CallType
import com.tnovoselec.truedetective.repository.CallRepository
import com.tnovoselec.truedetective.repository.PermissionRepository
import com.tnovoselec.truedetective.ui.calldetails.CallDetailsActivity
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Responsible for deciding if the call should be rejected, or let it go through.
 * If the call is normal or suspicious, let it through to CallDetailsActivity where more info about the call will be shown
 */
@Singleton
class CallHandler @Inject constructor(private val telephonyService: ITelephony,
                                      private val callResolver: CallResolver,
                                      private val callRelay: CallRelay,
                                      private val permissionRepository: PermissionRepository,
                                      private val callRepository: CallRepository,
                                      private val context: Context) {

    fun handleCall(state: String?, number: String?) {

        val formattedNumber = PhoneNumberUtils.formatNumber(number, "US")
        val callDescription = callResolver.resolve(state, formattedNumber).blockingGet()

        if (state.equals(TelephonyManager.EXTRA_STATE_RINGING)) {

            saveCall(callDescription)

            try {
                if (shouldEndCall(number, callDescription)) {
                    telephonyService.endCall()
                } else {
                    context.startActivity(Intent(context, CallDetailsActivity::class.java))
                }
            } catch (e: Exception) {
                Log.e("CallHandler", "Unable to end call", e)
            }

        }
        callRelay.callDescriptionRelay.onNext(callDescription)
    }

    private fun shouldEndCall(number: String?, callDescription: CallDescription): Boolean {
        return permissionRepository.getCallPhoneGranted() && number != null && callDescription.callType == CallType.SCAM
    }

    private fun saveCall(callDescription: CallDescription) {

        if (callDescription.number != null) {
            callRepository.saveCall(Call(callDescription.number, callDescription.callType, System.currentTimeMillis()))
                    .subscribeOn(Schedulers.io())
                    .subscribe()
        }
    }
}