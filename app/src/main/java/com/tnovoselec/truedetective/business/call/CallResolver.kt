package com.tnovoselec.truedetective.business.call

import android.telephony.TelephonyManager
import com.tnovoselec.truedetective.model.CallDescription
import com.tnovoselec.truedetective.model.CallState
import com.tnovoselec.truedetective.model.CallType
import com.tnovoselec.truedetective.model.PhoneContact
import com.tnovoselec.truedetective.repository.BlockListRepository
import com.tnovoselec.truedetective.repository.ContactRepository
import com.tnovoselec.truedetective.repository.NumberRepository
import io.reactivex.Single
import io.reactivex.functions.Function4
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Based on incoming number, current contact and block list, figure out the type of the call
 */
@Singleton
class CallResolver @Inject constructor(private val contactRepository: ContactRepository,
                                       private val numberRepository: NumberRepository,
                                       private val blockListRepository: BlockListRepository) {


    fun resolve(state: String?, number: String?): Single<CallDescription> {
        return Single.zip(numberRepository.getScamNumbers(),
                numberRepository.getSuspiciousNumbers(),
                blockListRepository.getBlockList(),
                contactRepository.getContacts(),
                Function4 { scamNumbers: List<String>,
                            suspiciousNumbers: List<String>,
                            blockList: List<String>,
                            contacts: List<PhoneContact> ->
                    resolve(state, number, scamNumbers, suspiciousNumbers, blockList, contacts)
                })

    }

    /*
     * If the number is in block list - mark it as scam in any case
     * If the number is marked as scam, but it's also in user's contact list - mark it as normal
     * Otherwise, if the number is in scam or suspicious lists, mark it appropriately
     */
    private fun resolve(state: String?,
                        number: String?,
                        scamNumbers: List<String>,
                        suspiciousNumbers: List<String>,
                        blockList: List<String>,
                        localContacts: List<PhoneContact>): CallDescription {

        val callState = when (state) {
            TelephonyManager.EXTRA_STATE_RINGING -> CallState.RINGING
            TelephonyManager.EXTRA_STATE_OFFHOOK -> CallState.OFF_HOOK
            else -> CallState.IDLE
        }

        return when (number) {
            in blockList -> CallDescription(state, number, CallType.SCAM, callState)
            in localContacts.map { it.number } -> CallDescription(state, number, CallType.NORMAL, callState)
            in suspiciousNumbers -> CallDescription(state, number, CallType.SUSPICIOUS, callState)
            in scamNumbers -> CallDescription(state, number, CallType.SCAM, callState)
            else -> CallDescription(state, number, CallType.NORMAL, callState)
        }
    }
}