package com.tnovoselec.truedetective.business.call

import com.tnovoselec.truedetective.model.CallDescription
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Bus to connect business and UI layer when necessary
 */
@Singleton
class CallRelay @Inject constructor() {

    val callDescriptionRelay: BehaviorSubject<CallDescription> = BehaviorSubject.create()
}