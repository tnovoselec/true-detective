package com.tnovoselec.truedetective.data.api

import com.tnovoselec.truedetective.data.api.model.CallerIdResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface CallerIdApi {

    @GET("v1/phone/{number}?account_sid=AC95916a2816d14780b2c38e3f69453512&auth_token=AU401aecb7c84e49e1ba7210fa625c3990&data=name,address,location,cnam,carrier,carrier_o,gender,linetype,image,line_provider,profile")
    fun getCallerId(@Path("number") number: String?): Single<CallerIdResponse>
}