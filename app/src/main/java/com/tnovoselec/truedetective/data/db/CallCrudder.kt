package com.tnovoselec.truedetective.data.db

import com.tnovoselec.truedetective.data.db.dao.CallDao
import com.tnovoselec.truedetective.data.db.model.DbCall
import com.tnovoselec.truedetective.model.Call
import com.tnovoselec.truedetective.model.CallType
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton


// Facade to interact with Room's classes, to ease the change of underlying implementation if necessary
@Singleton
class CallCrudder @Inject constructor(private val callDao: CallDao) {

    fun getCalls(): Single<List<Call>> {
        return Single.fromCallable { callDao.getCalls().map { Call(it.number, CallType.valueOf(it.type), it.timestamp) } }
    }

    fun saveCall(call: Call): Completable {
        return Completable.fromAction { callDao.saveCall(DbCall(0, call.number, call.callType.name, call.timeStamp)) }
    }
}