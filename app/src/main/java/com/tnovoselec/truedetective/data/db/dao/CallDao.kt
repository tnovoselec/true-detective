package com.tnovoselec.truedetective.data.db.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.tnovoselec.truedetective.data.db.model.DbCall


@Dao
interface CallDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveCall(call: DbCall)

    @Query("SELECT * FROM calls")
    fun getCalls(): List<DbCall>
}