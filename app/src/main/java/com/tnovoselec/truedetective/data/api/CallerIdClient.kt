package com.tnovoselec.truedetective.data.api

import com.tnovoselec.truedetective.data.api.model.CallerIdResponse
import com.tnovoselec.truedetective.model.CallerId
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CallerIdClient @Inject constructor(private val callerIdApi: CallerIdApi) {

    fun getCallerId(phoneNumber: String?): Single<CallerId> {
        return callerIdApi.getCallerId(phoneNumber)
                .map { toCallerId(it) }
    }

    private fun toCallerId(callerIdResponse: CallerIdResponse): CallerId {
        val name = callerIdResponse.data.name
        val location = callerIdResponse.data.location?.let {
            it.city + ", " + it.country
        }.orEmpty()

        return CallerId(name, location)
    }
}