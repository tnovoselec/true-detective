package com.tnovoselec.truedetective.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.tnovoselec.truedetective.data.db.dao.CallDao
import com.tnovoselec.truedetective.data.db.model.DbCall


@Database(version = TrueDetectiveDatabase.VERSION, entities = arrayOf(DbCall::class), exportSchema = false)
abstract class TrueDetectiveDatabase : RoomDatabase() {

    companion object {
        const val VERSION = 2
        const val NAME = "TrueDetectiveDatabase"
    }

    abstract fun callDao() : CallDao

}