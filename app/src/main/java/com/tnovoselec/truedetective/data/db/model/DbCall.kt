package com.tnovoselec.truedetective.data.db.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "calls")
data class DbCall(
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id: Long,
        @ColumnInfo(name = "number") val number: String,
        @ColumnInfo(name = "type") val type: String,
        @ColumnInfo(name = "timestamp") val timestamp: Long
)