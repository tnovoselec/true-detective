package com.tnovoselec.truedetective.data.api.model

data class CallerIdResponse(val data: Data)

data class Data(val name: String, val location: Location?)

data class Location(val city: String?, val country: String?, val state: String?)