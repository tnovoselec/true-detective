package com.tnovoselec.truedetective.ui.main.callhistory

import android.support.v4.content.ContextCompat
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.tnovoselec.truedetective.R
import com.tnovoselec.truedetective.model.Call
import com.tnovoselec.truedetective.model.CallType

class CallHistoryAdapter(private val inflater: LayoutInflater) : ListAdapter<Call, CallHistoryAdapter.CallViewHolder>(CallDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CallViewHolder {
        val view = inflater.inflate(R.layout.item_call_history, parent, false)
        return CallViewHolder(view)
    }

    override fun onBindViewHolder(holder: CallViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


    class CallViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.call_history_item_number)
        lateinit var number: TextView

        @BindView(R.id.call_history_item_type)
        lateinit var type: TextView

        fun bind(call: Call) {
            ButterKnife.bind(this, view)

            number.text = call.number
            type.text = call.callType.name

            number.setTextColor(ContextCompat.getColor(view.context, getCallColor(call)))
            type.setTextColor(ContextCompat.getColor(view.context, getCallColor(call)))
        }

        private fun getCallColor(call: Call): Int {

            return when (call.callType) {
                CallType.NORMAL -> R.color.call_type_color_normal
                CallType.SUSPICIOUS -> R.color.call_type_color_suspicious
                CallType.SCAM -> R.color.call_type_color_scam
            }

        }

    }

    class CallDiffCallback : DiffUtil.ItemCallback<Call>() {
        override fun areItemsTheSame(oldItem: Call?, newItem: Call?): Boolean {
            return oldItem?.number == newItem?.number
        }

        override fun areContentsTheSame(oldItem: Call?, newItem: Call?): Boolean {
            return oldItem == newItem
        }
    }
}