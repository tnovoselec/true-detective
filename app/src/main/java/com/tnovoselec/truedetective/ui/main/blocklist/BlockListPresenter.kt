package com.tnovoselec.truedetective.ui.main.blocklist

import com.tnovoselec.truedetective.repository.BlockListRepository
import com.tnovoselec.truedetective.ui.BasePresenter
import io.reactivex.Scheduler

class BlockListPresenter(private val blockListRepository: BlockListRepository,
                         private val subscribeOnScheduler: Scheduler,
                         private val observeOnScheduler: Scheduler) : BasePresenter<BlockListContract.View>(), BlockListContract.Presenter {


    override fun getBlockList() {
        addToUnsubscribe(
                blockListRepository.getBlockList()
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe({ onBlockList(it) }, { onBlockListError(it) })
        )
    }

    private fun onBlockList(blockList: List<String>) {
        view?.renderBlockList(blockList)
    }

    private fun onBlockListError(throwable: Throwable) {
        throwable.printStackTrace()
    }

    override fun addNumberToBlockList(number: String) {
        addToUnsubscribe(
                blockListRepository.addNumberToBlockList(number)
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe { onNumberAddedToBlockList() }
        )
    }

    private fun onNumberAddedToBlockList() {
        getBlockList()
    }

    override fun removeNumberFromBlockList(number: String) {
        addToUnsubscribe(
                blockListRepository.deleteNumberFromBlockList(number)
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe { onNumberRemovedFromBlockList() }
        )
    }

    private fun onNumberRemovedFromBlockList() {
        getBlockList()
    }

}