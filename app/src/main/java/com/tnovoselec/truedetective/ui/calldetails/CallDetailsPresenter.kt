package com.tnovoselec.truedetective.ui.calldetails

import android.telephony.PhoneNumberUtils
import com.tnovoselec.truedetective.business.call.CallRelay
import com.tnovoselec.truedetective.model.CallDescription
import com.tnovoselec.truedetective.model.CallState
import com.tnovoselec.truedetective.model.CallType
import com.tnovoselec.truedetective.model.CallerId
import com.tnovoselec.truedetective.repository.CallerIdRepository
import com.tnovoselec.truedetective.ui.BasePresenter
import io.reactivex.Scheduler

class CallDetailsPresenter(private val callerIdRepository: CallerIdRepository,
                           private val callRelay: CallRelay,
                           private val subscribeOnScheduler: Scheduler,
                           private val observeOnScheduler: Scheduler) : BasePresenter<CallDetailsContract.View>(), CallDetailsContract.Presenter {

    override fun register(view: CallDetailsContract.View) {
        super.register(view)
        addToUnsubscribe(callRelay.callDescriptionRelay.subscribe { onCall(it) })
    }

    override fun getCallerId(number: String?) {

        val e164PhoneNumber = PhoneNumberUtils.formatNumberToE164(number, "US")

        addToUnsubscribe(
                callerIdRepository.getCallerId(e164PhoneNumber)
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe({ onCallerId(it) }, { onCallerIdError(it) })
        )
    }

    private fun onCall(callDescription: CallDescription) {
        when (callDescription.callState) {
            CallState.RINGING -> view?.renderCall(callDescription)
            CallState.IDLE -> view?.close()
            else -> { }//DO NOTHING
        }

        if (callDescription.callType == CallType.NORMAL && callDescription.callState == CallState.RINGING){
            getCallerId(callDescription.number)
        }

    }

    private fun onCallerId(callerId: CallerId) {
        if (!callerId.name.isNullOrEmpty()) {
            view?.renderCallerId(callerId)
        }
    }

    private fun onCallerIdError(throwable: Throwable) {
        throwable.printStackTrace()
    }
}