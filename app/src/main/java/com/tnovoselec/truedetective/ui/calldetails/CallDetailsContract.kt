package com.tnovoselec.truedetective.ui.calldetails

import com.tnovoselec.truedetective.model.CallDescription
import com.tnovoselec.truedetective.model.CallerId
import com.tnovoselec.truedetective.ui.BasePresenterView

interface CallDetailsContract {


    interface View : BasePresenterView {

        fun renderCallerId(callerId: CallerId)
        fun renderCall(callDescription: CallDescription)
        fun close()
    }

    interface Presenter {
        fun getCallerId(number: String?)
    }
}