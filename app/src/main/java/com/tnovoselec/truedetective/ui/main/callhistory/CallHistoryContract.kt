package com.tnovoselec.truedetective.ui.main.callhistory

import com.tnovoselec.truedetective.model.Call
import com.tnovoselec.truedetective.ui.BasePresenterView

interface CallHistoryContract {

    interface View : BasePresenterView {
        fun renderCallHistory(calls: List<Call>)
    }

    interface Presenter {
        fun getCallHistory()
    }
}