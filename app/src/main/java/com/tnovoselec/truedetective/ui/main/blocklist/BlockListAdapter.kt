package com.tnovoselec.truedetective.ui.main.blocklist

import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.tnovoselec.truedetective.R

class BlockListAdapter(private val inflater: LayoutInflater,
                       private val onItemClickListener: OnItemClickListener) : ListAdapter<String, BlockListAdapter.BlockListViewHolder>(BlockListAdapter.BlockListCallback()) {

    interface OnItemClickListener {
        fun onItemClicked(number: String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BlockListAdapter.BlockListViewHolder {
        val view = inflater.inflate(R.layout.item_block_list, parent, false)
        return BlockListViewHolder(view)
    }

    override fun onBindViewHolder(holder: BlockListAdapter.BlockListViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    inner class BlockListViewHolder(val view: View) : RecyclerView.ViewHolder(view) {

        @BindView(R.id.block_list_number)
        lateinit var numberLabel: TextView


        fun bind(number: String) {
            ButterKnife.bind(this, view)

            numberLabel.text = number
            view.setOnClickListener { onItemClickListener.onItemClicked(number) }
        }

    }

    class BlockListCallback : DiffUtil.ItemCallback<String>() {
        override fun areItemsTheSame(oldItem: String?, newItem: String?): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: String?, newItem: String?): Boolean {
            return oldItem == newItem
        }
    }
}