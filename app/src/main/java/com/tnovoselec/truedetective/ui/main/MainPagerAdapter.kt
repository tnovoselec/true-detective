package com.tnovoselec.truedetective.ui.main

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.tnovoselec.truedetective.ui.main.blocklist.BlockListFragment
import com.tnovoselec.truedetective.ui.main.callhistory.CallHistoryFragment

class MainPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> CallHistoryFragment()
            1 -> BlockListFragment()
            else -> Fragment()
        }
    }

    override fun getCount() = 2

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Call History"
            1 -> "Block List"
            else -> ""
        }
    }
}