package com.tnovoselec.truedetective.ui.main.blocklist

import android.app.AlertDialog
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.tnovoselec.truedetective.R
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class BlockListFragment : DaggerFragment(), BlockListContract.View, BlockListAdapter.OnItemClickListener {


    @BindView(R.id.block_list)
    lateinit var blockList: RecyclerView

    @BindView(R.id.block_list_empty)
    lateinit var emptyList: TextView

    @Inject
    lateinit var blockListPresenter: BlockListPresenter

    lateinit var blockListAdapter: BlockListAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_block_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)

        blockList.layoutManager = LinearLayoutManager(context)
        blockList.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        blockListAdapter = BlockListAdapter(layoutInflater, this)
        blockList.adapter = blockListAdapter
    }

    override fun onStart() {
        super.onStart()
        blockListPresenter.register(this)
        blockListPresenter.getBlockList()
    }

    override fun onStop() {
        super.onStop()
        blockListPresenter.unregister()
    }

    override fun renderBlockList(numbers: List<String>) {
        if (numbers.isEmpty()) {
            blockList.visibility = View.GONE
            emptyList.visibility = View.VISIBLE
        } else {
            blockListAdapter.submitList(numbers)
            blockList.visibility = View.VISIBLE
            emptyList.visibility = View.GONE
        }
    }

    @OnClick(R.id.block_list_fab)
    fun onFabClicked() {
        showAddNumberDialog()
    }

    override fun onItemClicked(number: String) {
        showRemoveNumberDialog(number)
    }

    private fun showAddNumberDialog() {
        val alert = AlertDialog.Builder(context)
        alert.setTitle(getString(R.string.add_number_to_block_list))

        val input = EditText(context)
        input.inputType = InputType.TYPE_CLASS_PHONE

        alert.setView(input)
        alert.setPositiveButton(getString(R.string.ok)) { _, _ ->
            blockListPresenter.addNumberToBlockList(input.text.toString())
        }
        alert.setNegativeButton(getString(R.string.cancel)) { _, _ ->
            //Dismiss
        }
        val dialog = alert.create()
        dialog.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        dialog.show()
    }



    private fun showRemoveNumberDialog(number: String) {
        val alert = AlertDialog.Builder(context)
        alert.setTitle(getString(R.string.remove_number_from_block_list, number))

        alert.setPositiveButton(getString(R.string.ok)) { _, _ ->
            blockListPresenter.removeNumberFromBlockList(number)
        }
        alert.setNegativeButton(getString(R.string.cancel)) { _, _ ->
            //Dismiss
        }
        alert.show()
    }

}