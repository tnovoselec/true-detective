package com.tnovoselec.truedetective.ui.main.callhistory

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.tnovoselec.truedetective.R
import com.tnovoselec.truedetective.model.Call
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CallHistoryFragment : DaggerFragment(), CallHistoryContract.View {


    @BindView(R.id.call_history_list)
    lateinit var callHistoryList: RecyclerView

    @BindView(R.id.call_history_empty)
    lateinit var emptyList: TextView

    @Inject
    lateinit var callHistoryPresenter: CallHistoryPresenter

    lateinit var callHistoryAdapter: CallHistoryAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_call_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ButterKnife.bind(this, view)

        callHistoryList.layoutManager = LinearLayoutManager(context)
        callHistoryAdapter = CallHistoryAdapter(layoutInflater)
        callHistoryList.adapter = callHistoryAdapter
    }

    override fun onStart() {
        super.onStart()
        callHistoryPresenter.register(this)
        callHistoryPresenter.getCallHistory()
    }

    override fun onStop() {
        super.onStop()
        callHistoryPresenter.unregister()
    }

    override fun renderCallHistory(calls: List<Call>) {

        if (calls.isEmpty()) {
            callHistoryList.visibility = View.GONE
            emptyList.visibility = View.VISIBLE
        } else {
            callHistoryAdapter.submitList(calls)
            callHistoryList.visibility = View.VISIBLE
            emptyList.visibility = View.GONE
        }
    }
}