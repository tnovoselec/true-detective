package com.tnovoselec.truedetective.ui.main.callhistory

import com.tnovoselec.truedetective.model.Call
import com.tnovoselec.truedetective.repository.CallRepository
import com.tnovoselec.truedetective.ui.BasePresenter
import io.reactivex.Scheduler

class CallHistoryPresenter(private val callRepository: CallRepository,
                           private val subscribeOnScheduler: Scheduler,
                           private val observeOnScheduler: Scheduler) : BasePresenter<CallHistoryContract.View>(), CallHistoryContract.Presenter {


    override fun getCallHistory() {
        addToUnsubscribe(
                callRepository.getCalls()
                        .subscribeOn(subscribeOnScheduler)
                        .observeOn(observeOnScheduler)
                        .subscribe({ onCalls(it) }, { onCallsError(it) })
        )
    }

    private fun onCalls(calls: List<Call>) {
        view?.renderCallHistory(calls)
    }

    private fun onCallsError(throwable: Throwable) {
        throwable.printStackTrace()
    }
}