package com.tnovoselec.truedetective.ui.main.blocklist

import com.tnovoselec.truedetective.ui.BasePresenterView

interface BlockListContract {

    interface View : BasePresenterView {
        fun renderBlockList(numbers: List<String>)
    }

    interface Presenter {
        fun getBlockList()
        fun addNumberToBlockList(number:String)
        fun removeNumberFromBlockList(number:String)
    }
}