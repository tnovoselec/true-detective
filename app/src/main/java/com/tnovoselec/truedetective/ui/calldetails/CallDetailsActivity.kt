package com.tnovoselec.truedetective.ui.calldetails

import android.app.Activity
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.widget.TextView
import android.widget.Toast
import com.tnovoselec.truedetective.R
import com.tnovoselec.truedetective.model.CallDescription
import com.tnovoselec.truedetective.model.CallType
import com.tnovoselec.truedetective.model.CallerId
import dagger.android.AndroidInjection
import javax.inject.Inject


class CallDetailsActivity : Activity(), CallDetailsContract.View {


    @Inject
    lateinit var callDetailsPresenter: CallDetailsPresenter

    private var toast: Toast? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);


        AndroidInjection.inject(this)
        callDetailsPresenter.register(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        callDetailsPresenter.unregister()
        toast?.cancel()
    }

    override fun renderCallerId(callerId: CallerId) {
        showCallerId(callerId)
    }

    override fun renderCall(callDescription: CallDescription) {
        if (callDescription.callType == CallType.SUSPICIOUS) {
            showWarningMessage()
        }
    }

    override fun close() {
        finish()
    }

    private fun showWarningMessage() {
        repeat(3) {
            toast = Toast(applicationContext)
            toast?.setGravity(Gravity.BOTTOM, 0, 0)
            toast?.duration = Toast.LENGTH_SHORT
            toast?.view = layoutInflater.inflate(R.layout.toast_warning_message, null)
            toast?.show()
        }
    }

    private fun showCallerId(callerId: CallerId) {
        repeat(3) {
            toast = Toast(applicationContext)
            toast?.setGravity(Gravity.BOTTOM, 0, 0)
            toast?.duration = Toast.LENGTH_SHORT
            val view = layoutInflater.inflate(R.layout.toast_caller_id_message, null)
            val nameTextView = view.findViewById<TextView>(R.id.toast_caller_id_name)
            val locationTextView = view.findViewById<TextView>(R.id.toast_caller_id_location)

            nameTextView.text = getString(R.string.incoming_call_from, callerId.name)

            if (!callerId.location.isNullOrEmpty()) {
                locationTextView.text = getString(R.string.incoming_call_location, callerId.location)
            } else {
                locationTextView.visibility = View.GONE
            }

            toast?.view = view
            toast?.show()
        }
    }


}