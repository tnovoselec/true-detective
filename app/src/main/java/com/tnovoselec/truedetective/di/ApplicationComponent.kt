package com.tnovoselec.truedetective.di

import com.tnovoselec.truedetective.TrueDetectiveApplication
import dagger.BindsInstance
import dagger.Component
import dagger.MembersInjector
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            ApplicationModule::class,
            NetworkModule::class,
            ThreadingModule::class,
            DataModule::class,
            AndroidInjectionModule::class,
            ActivityBuilder::class,
            BroadcastReceiverBuilder::class
        ]
)
interface ApplicationComponent : MembersInjector<TrueDetectiveApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: TrueDetectiveApplication): Builder

        fun build(): ApplicationComponent
    }
}