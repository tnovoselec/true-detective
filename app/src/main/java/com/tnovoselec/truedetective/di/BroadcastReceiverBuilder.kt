package com.tnovoselec.truedetective.di

import com.tnovoselec.truedetective.business.call.CallReceiver
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class BroadcastReceiverBuilder {

    @ContributesAndroidInjector(modules = [BroadcastReceiverModule::class])
    abstract fun bindCallReceiver() : CallReceiver
}