package com.tnovoselec.truedetective.di

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.telephony.TelephonyManager
import com.android.internal.telephony.ITelephony
import com.tnovoselec.truedetective.TrueDetectiveApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ApplicationModule {

    @Provides
    @Singleton
    fun provideContext(application: TrueDetectiveApplication): Context {
        return application
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(application: TrueDetectiveApplication): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(application)
    }

    @Provides
    @Singleton
    fun provideTelephonyService(application: TrueDetectiveApplication): ITelephony {
        val telephonyManager = application.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        val declaredMethod = telephonyManager.javaClass.getDeclaredMethod("getITelephony")

        declaredMethod.isAccessible = true
        return declaredMethod.invoke(telephonyManager) as ITelephony
    }
}