package com.tnovoselec.truedetective.di

import com.tnovoselec.truedetective.ui.calldetails.CallDetailsActivity
import com.tnovoselec.truedetective.ui.main.MainActivity
import com.tnovoselec.truedetective.ui.main.blocklist.BlockListFragment
import com.tnovoselec.truedetective.ui.main.callhistory.CallHistoryFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector


@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindMainActivity() : MainActivity

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindCallDetailsActivity() : CallDetailsActivity

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindCallHistoryFragment() : CallHistoryFragment

    @ContributesAndroidInjector(modules = [ActivityModule::class])
    abstract fun bindBlockListFragment() : BlockListFragment

}