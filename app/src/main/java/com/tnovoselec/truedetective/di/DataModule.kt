package com.tnovoselec.truedetective.di

import android.arch.persistence.room.Room
import android.content.Context
import com.tnovoselec.truedetective.data.db.TrueDetectiveDatabase
import com.tnovoselec.truedetective.repository.ContactRepository
import com.tnovoselec.truedetective.repository.LocalContactRepository
import com.tnovoselec.truedetective.repository.LocalNumberRepository
import com.tnovoselec.truedetective.repository.NumberRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/*
 * Module responsible for providing data sources. Here we can change the implementation of NumberRepository and ContactRepository as we like.
 */
@Module
class DataModule {

    @Provides
    @Singleton
    fun provideNumberRepository(localNumberRepository: LocalNumberRepository): NumberRepository = localNumberRepository

    @Provides
    @Singleton
    fun provideContactRepository(localContactRepository: LocalContactRepository): ContactRepository = localContactRepository

    @Provides
    @Singleton
    fun provideTrueDetectiveDatabase(context: Context): TrueDetectiveDatabase = Room.databaseBuilder(context, TrueDetectiveDatabase::class.java, TrueDetectiveDatabase.NAME).build()

    @Provides
    @Singleton
    fun provideCallDao(trueDetectiveDatabase: TrueDetectiveDatabase) = trueDetectiveDatabase.callDao()
}