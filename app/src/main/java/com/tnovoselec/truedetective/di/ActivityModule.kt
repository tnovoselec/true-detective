package com.tnovoselec.truedetective.di

import com.tnovoselec.truedetective.business.call.CallRelay
import com.tnovoselec.truedetective.repository.BlockListRepository
import com.tnovoselec.truedetective.repository.CallRepository
import com.tnovoselec.truedetective.repository.CallerIdRepository
import com.tnovoselec.truedetective.ui.calldetails.CallDetailsPresenter
import com.tnovoselec.truedetective.ui.main.blocklist.BlockListPresenter
import com.tnovoselec.truedetective.ui.main.callhistory.CallHistoryPresenter
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import javax.inject.Named


@Module
class ActivityModule {

    @Provides
    fun provideCallDetailsPresenter(@Named(ThreadingModule.MAIN_SCHEDULER) mainThreadScheduler: Scheduler,
                                    @Named(ThreadingModule.BACKGROUND_SCHEDULER) backgroundScheduler: Scheduler,
                                    callerIdRepository: CallerIdRepository,
                                    callRelay: CallRelay): CallDetailsPresenter {
        return CallDetailsPresenter(callerIdRepository, callRelay, backgroundScheduler, mainThreadScheduler)
    }

    @Provides
    fun provideCallHistoryPresenter(@Named(ThreadingModule.MAIN_SCHEDULER) mainThreadScheduler: Scheduler,
                                    @Named(ThreadingModule.BACKGROUND_SCHEDULER) backgroundScheduler: Scheduler,
                                    callRepository: CallRepository): CallHistoryPresenter {
        return CallHistoryPresenter(callRepository, backgroundScheduler, mainThreadScheduler)
    }

    @Provides
    fun provideBlockListPresenter(@Named(ThreadingModule.MAIN_SCHEDULER) mainThreadScheduler: Scheduler,
                                  @Named(ThreadingModule.BACKGROUND_SCHEDULER) backgroundScheduler: Scheduler,
                                  blockListRepository: BlockListRepository): BlockListPresenter {
        return BlockListPresenter(blockListRepository, backgroundScheduler, mainThreadScheduler)
    }
}