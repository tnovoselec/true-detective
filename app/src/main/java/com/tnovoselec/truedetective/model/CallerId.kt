package com.tnovoselec.truedetective.model

data class CallerId(val name: String?, val location: String?)