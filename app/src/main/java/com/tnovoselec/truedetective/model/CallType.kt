package com.tnovoselec.truedetective.model

enum class CallType {
    NORMAL,
    SUSPICIOUS,
    SCAM
}