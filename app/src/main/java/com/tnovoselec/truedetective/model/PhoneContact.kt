package com.tnovoselec.truedetective.model

data class PhoneContact(val number: String, val name: String)