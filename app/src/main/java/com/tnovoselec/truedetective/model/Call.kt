package com.tnovoselec.truedetective.model

data class Call(val number: String,
                val callType: CallType,
                val timeStamp:Long)