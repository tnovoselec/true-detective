package com.tnovoselec.truedetective.model

data class CallDescription(val state: String?,
                           val number: String?,
                           val callType: CallType,
                           val callState: CallState)