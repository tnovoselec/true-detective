package com.tnovoselec.truedetective.model

enum class CallState {

    RINGING,
    IDLE,
    OFF_HOOK
}