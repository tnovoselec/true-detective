package com.tnovoselec.truedetective

import android.app.Activity
import android.app.Application
import android.content.BroadcastReceiver
import android.support.v4.app.Fragment
import com.facebook.stetho.Stetho
import com.tnovoselec.truedetective.di.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasBroadcastReceiverInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject

class TrueDetectiveApplication : Application(), HasActivityInjector, HasBroadcastReceiverInjector, HasSupportFragmentInjector {

    @Inject
    lateinit var broadCastReceiverDispatchingAndroidInjector: DispatchingAndroidInjector<BroadcastReceiver>

    @Inject
    lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    lateinit var supportFragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector(): AndroidInjector<Activity> = activityDispatchingAndroidInjector
    override fun supportFragmentInjector(): AndroidInjector<Fragment> = supportFragmentDispatchingAndroidInjector
    override fun broadcastReceiverInjector(): AndroidInjector<BroadcastReceiver> = broadCastReceiverDispatchingAndroidInjector

    override fun onCreate() {
        super.onCreate()
        DaggerApplicationComponent.builder().application(this).build().injectMembers(this)
        Stetho.initializeWithDefaults(this)
    }
}