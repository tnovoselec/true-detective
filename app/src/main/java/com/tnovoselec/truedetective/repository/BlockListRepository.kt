package com.tnovoselec.truedetective.repository

import android.content.SharedPreferences
import android.telephony.PhoneNumberUtils
import io.reactivex.Completable
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Repository for storing/fetching locally saved numbers
 */
@Singleton
class BlockListRepository @Inject constructor(private val sharedPreferences: SharedPreferences) {

    companion object {
        val KEY_BLOCKED_NUMBERS = "blocked_numbers"
    }

    fun getBlockList(): Single<List<String>> {
        return Single.fromCallable {
            sharedPreferences.getStringSet(KEY_BLOCKED_NUMBERS, emptySet()).toList()
        }
    }

    fun addNumberToBlockList(number: String): Completable {
        return getBlockList().flatMapCompletable {
            val numbers = it.toMutableList()
            numbers.add(PhoneNumberUtils.formatNumber(number, "US") )
            sharedPreferences.edit().putStringSet(KEY_BLOCKED_NUMBERS, numbers.toHashSet()).apply()
            Completable.complete()
        }

    }

    fun deleteNumberFromBlockList(number: String): Completable {
        return getBlockList().flatMapCompletable {
            val numbers = it.toMutableList()
            numbers.remove(number)
            sharedPreferences.edit().putStringSet(KEY_BLOCKED_NUMBERS, numbers.toHashSet()).apply()
            Completable.complete()
        }
    }
}