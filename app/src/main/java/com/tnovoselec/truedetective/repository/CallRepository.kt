package com.tnovoselec.truedetective.repository

import com.tnovoselec.truedetective.data.db.CallCrudder
import com.tnovoselec.truedetective.model.Call
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

// Repository for storing all of incoming calls
@Singleton
class CallRepository @Inject constructor(private val callCrudder: CallCrudder) {

    fun getCalls(): Single<List<Call>> {
        return callCrudder.getCalls()
    }

    fun saveCall(call: Call) = callCrudder.saveCall(call)
}