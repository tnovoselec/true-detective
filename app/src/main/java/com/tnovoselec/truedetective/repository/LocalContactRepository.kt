package com.tnovoselec.truedetective.repository

import android.content.Context
import android.provider.ContactsContract
import com.tnovoselec.truedetective.model.PhoneContact
import io.reactivex.Single
import javax.inject.Inject

/*
 * Concrete implementation of ContactRepository where contacts are queried directly from device. In future, we might wanna get Contacts from some other service
 */
class LocalContactRepository @Inject constructor(private val context: Context,
                                                 private val permissionRepository: PermissionRepository) : ContactRepository {

    override fun getContacts(): Single<List<PhoneContact>> {
        if (permissionRepository.getReadContactsGranted()) {
            return Single.fromCallable { readContacts() }
        } else {
            return Single.just(emptyList())
        }
    }

    private fun readContacts(): List<PhoneContact> {
        val contacts: MutableList<PhoneContact> = mutableListOf()

        val contentResolver = context.contentResolver
        val phones = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null)

        while (phones.moveToNext()) {
            val name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            val number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))

            contacts.add(PhoneContact(number, name))
        }

        phones.close()

        return contacts
    }
}