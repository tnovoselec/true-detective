package com.tnovoselec.truedetective.repository

import io.reactivex.Single

/*
 * Interface to define how the different kind of numbers are fetched
 */
interface NumberRepository {

    fun getSuspiciousNumbers(): Single<List<String>>

    fun getScamNumbers(): Single<List<String>>
}