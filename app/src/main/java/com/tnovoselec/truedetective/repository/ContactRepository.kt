package com.tnovoselec.truedetective.repository

import com.tnovoselec.truedetective.model.PhoneContact
import io.reactivex.Single

/*
 * Interface to define how the users will be fetched.
 */
interface ContactRepository {

    fun getContacts(): Single<List<PhoneContact>>
}