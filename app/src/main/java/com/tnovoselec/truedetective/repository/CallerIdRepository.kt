package com.tnovoselec.truedetective.repository

import com.tnovoselec.truedetective.data.api.CallerIdClient
import com.tnovoselec.truedetective.model.CallerId
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

/*
 * Repository for interacting with public available CNAM service. This could be hidden behind the interface to ease the change of internal implementation
 */
@Singleton
class CallerIdRepository @Inject constructor(private val callerIdClient: CallerIdClient) {

    fun getCallerId(number: String?): Single<CallerId> {
        return callerIdClient.getCallerId(number)
//        return Single.just(getMockCallerId())
    }

    fun getMockCallerId() = CallerId("John Doe", "Zagreb, HR")
}