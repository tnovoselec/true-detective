package com.tnovoselec.truedetective.repository

import android.telephony.PhoneNumberUtils
import io.reactivex.Single
import javax.inject.Inject

/*
 * Concrete implementation of NumberRepository where scam and suspicious numbers are hardcoded in the app.
 * We can easily switch this with RemoteNumberRepository where the numbers are fetched from Teltech servers or something similar
 */
class LocalNumberRepository @Inject constructor() : NumberRepository {

    companion object {
        val SUSPICIOUS_NUMBERS = arrayListOf("4259501212").map { PhoneNumberUtils.formatNumber(it, "US") }
        val SCAM_NUMBERS = arrayListOf("2539501212").map { PhoneNumberUtils.formatNumber(it, "US") }
    }

    override fun getSuspiciousNumbers(): Single<List<String>> {
        return Single.just(SUSPICIOUS_NUMBERS)
    }

    override fun getScamNumbers(): Single<List<String>> {
        return Single.just(SCAM_NUMBERS)
    }
}